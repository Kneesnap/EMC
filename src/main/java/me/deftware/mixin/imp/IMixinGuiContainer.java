package me.deftware.mixin.imp;

import net.minecraft.container.Slot;

public interface IMixinGuiContainer {

    Slot getHoveredSlot();

}
